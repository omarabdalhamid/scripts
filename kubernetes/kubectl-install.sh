#!/bin/bash
################################################################################
# Script for installing kubectl 
# Author: OmarAbdalhamid Omar
#-------------------------------------------------------------------------------
# This script will install kubectl  on your linuxserver. I
#-------------------------------------------------------------------------------
# Make a new file:
# sudo nano kubectl-install.sh
# Place this content in it and then make the file executable:
# sudo chmod +x kubectl-install.sh
# Execute the script to install kubectl:
# bash <(curl -s https://gitlab.com/omarabdalhamid/scripts/-/raw/main/kubernetes/kubectl-install.sh)
################################################################################

# Function to install kubectl on Red Hat based distributions
install_kubectl_redhat() {
    sudo yum install -y curl
    sudo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    sudo chmod +x kubectl
    sudo mv kubectl /usr/local/bin/
}

# Function to install kubectl on Debian based distributions
install_kubectl_debian() {
    sudo apt-get update
    sudo apt-get install -y curl
    sudo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    sudo chmod +x kubectl
    sudo mv kubectl /usr/local/bin/
}

# Function to install kubectl on SUSE based distributions
install_kubectl_suse() {
    sudo zypper install -y curl
    sudo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    sudo chmod +x kubectl
    sudo mv kubectl /usr/local/bin/
}

# Function to install kubectl on Ubuntu
install_kubectl_ubuntu() {
    sudo apt-get update
    sudo apt-get install -y curl
    sudo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    sudo chmod +x kubectl
    sudo mv kubectl /usr/local/bin/
}

# Detect the Linux distribution
detect_linux_distribution() {
    if [ -f /etc/redhat-release ]; then
        install_kubectl_redhat
    elif [ -f /etc/debian_version ]; then
        install_kubectl_debian
    elif [ -f /etc/SuSE-release ]; then
        install_kubectl_suse
    elif [ -f /etc/lsb-release ]; then
        install_kubectl_ubuntu
    else
        echo "Unsupported Linux distribution."
        exit 1
    fi
}

# Call the function to detect and install kubectl
detect_linux_distribution