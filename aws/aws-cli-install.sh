#!/bin/bash
################################################################################
# Script for installing aws-cli 
# Author: OmarAbdalhamid Omar
#-------------------------------------------------------------------------------
# This script will install aws-cli  on your linuxserver. I
#-------------------------------------------------------------------------------
# Make a new file:
# sudo nano aws-cli-install.sh
# Place this content in it and then make the file executable:
# sudo chmod +x aws-cli-install.sh
# Execute the script to install aws-cli:
# bash <(curl -s https://gitlab.com/omarabdalhamid/scripts/-/raw/main/aws/aws-cli-install.sh)
################################################################################

# Function to install AWS CLI on Red Hat based distributions
install_awscli_redhat() {
    sudo yum install -y curl unzip
    curl "https://d1vvhvl2y92vvt.cloudfront.net/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
    rm -rf aws awscliv2.zip
}

# Function to install AWS CLI on Debian based distributions
install_awscli_debian() {
    sudo apt-get update
    sudo apt-get install -y curl unzip
    curl "https://d1vvhvl2y92vvt.cloudfront.net/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
    rm -rf aws awscliv2.zip
}

# Function to install AWS CLI on SUSE based distributions
install_awscli_suse() {
    sudo zypper install -y curl unzip
    curl "https://d1vvhvl2y92vvt.cloudfront.net/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
    rm -rf aws awscliv2.zip
}

# Function to install AWS CLI on Ubuntu
install_awscli_ubuntu() {
    sudo apt-get update
    sudo apt-get install -y curl unzip
    curl "https://d1vvhvl2y92vvt.cloudfront.net/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
    rm -rf aws awscliv2.zip
}

# Detect the Linux distribution
detect_linux_distribution() {
    if [ -f /etc/redhat-release ]; then
        install_awscli_redhat
    elif [ -f /etc/debian_version ]; then
        install_awscli_debian
    elif [ -f /etc/SuSE-release ]; then
        install_awscli_suse
    elif [ -f /etc/lsb-release ]; then
        install_awscli_ubuntu
    else
        echo "Unsupported Linux distribution."
        exit 1
    fi
}

# Call the function to detect and install AWS CLI
detect_linux_distribution