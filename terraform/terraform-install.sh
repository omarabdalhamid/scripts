#!/bin/bash
################################################################################
# Script for installing Terraform 
# Author: OmarAbdalhamid Omar
#-------------------------------------------------------------------------------
# This script will install terraform  on your linuxserver. I
#-------------------------------------------------------------------------------
# Make a new file:
# sudo nano terraform-install.sh
# Place this content in it and then make the file executable:
# sudo chmod +x terraform-install.sh
# Execute the script to install terraform:
# bash <(curl -s https://gitlab.com/omarabdalhamid/scripts/-/raw/main/terraform/terraform-install.sh)
################################################################################


# Function to install Terraform on Red Hat based distributions
install_terraform_redhat() {
    sudo dnf install -y curl unzip
    curl -LO https://releases.hashicorp.com/terraform/1.6.5/terraform_1.6.5_linux_amd64.zip
    unzip terraform_1.6.5_linux_amd64.zip
    sudo mv terraform /usr/local/bin/
    rm terraform_1.6.5_linux_amd64.zip
}

# Function to install Terraform on Debian based distributions
install_terraform_debian() {
    sudo apt-get update
    sudo apt-get install -y curl unzip
    curl -LO https://releases.hashicorp.com/terraform/1.6.5/terraform_1.6.5_linux_amd64.zip
    unzip terraform_1.6.5_linux_amd64.zip
    sudo mv terraform /usr/local/bin/
    rm terraform_1.6.5_linux_amd64.zip
}

# Function to install Terraform on SUSE based distributions
install_terraform_suse() {
    sudo zypper install -y curl unzip
    curl -LO https://releases.hashicorp.com/terraform/1.6.5/terraform_1.6.5_linux_amd64.zip
    unzip terraform_1.6.5_linux_amd64.zip
    sudo mv terraform /usr/local/bin/
    rm terraform_1.6.5_linux_amd64.zip
}

# Function to install Terraform on Ubuntu
install_terraform_ubuntu() {
    sudo apt-get update
    sudo apt-get install -y curl unzip
    curl -LO https://releases.hashicorp.com/terraform/1.6.5/terraform_1.6.5_linux_amd64.zip
    unzip terraform_1.6.5_linux_amd64.zip
    sudo mv terraform /usr/local/bin/
    rm terraform_1.6.5_linux_amd64.zip
}

# Detect the Linux distribution
detect_linux_distribution() {
    if [ -f /etc/redhat-release ]; then
        install_terraform_redhat
    elif [ -f /etc/debian_version ]; then
        install_terraform_debian
    elif [ -f /etc/SuSE-release ]; then
        install_terraform_suse
    elif [ -f /etc/lsb-release ]; then
        install_terraform_ubuntu
    else
        echo "Unsupported Linux distribution."
        exit 1
    fi
}

# Call the function to detect and install Terraform
detect_linux_distribution
